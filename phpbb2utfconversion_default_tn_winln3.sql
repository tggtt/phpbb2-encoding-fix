/**
CONVERSION SCRIPT.
NOTICE: THE TABLE NAMES VARY.

This file is ASCII.

Defaults are:
1-  POSTS_TABLE or phpbb_posts
2-  TOPICS_TABLE or phpbb_topics
3-  POSTS_TEXT_TABLE or phpbb_posts_text

WARNING: Version 5 is recommended. This leads to several warnings I cannot check.

*/

SET @FIRST_POST_AFTER := (<<FIRST POST NUMBER AFTER MAINTENANCE HERE>>);


/*ACTUAL CONVERSION*/

/*convert the whole posts_text table. */

/*warning: LD4all has more than 444000 posts, this could take a long time! */

/**********POST TEXT SUBJECT******************/
ALTER TABLE `phpbb_posts_text` CHANGE COLUMN post_subject post_subject CHAR(60) CHARACTER SET 'binary';
UPDATE
    `phpbb_posts_text` post_t_t
  SET post_subject = @txt 
  WHERE 
  (char_length(post_subject) <>  LENGTH(@txt := CONVERT(CONVERT(post_subject USING latin1) USING utf8)))
  and
  (post_t_t.post_id 
   IN
   (select posts_older.post_id
     from `phpbb_posts` posts_older inner join `phpbb_posts` posts_t
     on posts_older.post_time < posts_t.post_time
     where (posts_t.post_id = @FIRST_POST_AFTER)
   )
  ) /* all valid posts below new*/
  and
  (post_t_t.post_id >= 1) /* safe mode compliance */
;
ALTER TABLE `phpbb_posts_text` CHANGE COLUMN post_subject post_subject CHAR(60) CHARACTER SET 'utf8';

/**********POST TEXT BODY******************/
ALTER TABLE `phpbb_posts_text` CHANGE COLUMN post_text post_text TEXT CHARACTER SET 'binary';
UPDATE
    `phpbb_posts_text` post_t_t
  SET post_text = @txt 
  WHERE 
  (char_length(post_text) <>  LENGTH(@txt := CONVERT(CONVERT(post_text USING latin1) USING utf8)))
  and
  (post_t_t.post_id 
   IN
   (select posts_older.post_id
     from `phpbb_posts` posts_older inner join `phpbb_posts` posts_t
     on posts_older.post_time < posts_t.post_time
     where (posts_t.post_id = @FIRST_POST_AFTER)
   )
  ) /* all valid posts below new*/
  and
  (post_t_t.post_id >= 1) /* safe mode compliance */
;
ALTER TABLE `phpbb_posts_text` CHANGE COLUMN post_text post_text TEXT CHARACTER SET 'utf8';

/*********TOPIC TITLE********/

ALTER TABLE `phpbb_topics` CHANGE COLUMN topic_title topic_title CHAR(60) CHARACTER SET 'binary';
UPDATE
    `phpbb_topics` topics_t
  SET topic_title = @txt
  where
  LENGTH(topic_title) <> (@txt:= CONVERT(CONVERT(topic_title USING latin1) USING utf8))
  and
  topics_t.topic_time  /* SAME OR NEWER than projected first post*/
  <
 (
  select post_time from `phpbb_posts` posts_t where (posts_t.post_id = @FIRST_POST_AFTER) /* all posts older than first */
 )
 and
 (topics_t.topic_id >= 1) /* safe mode compliance */
;
ALTER TABLE `phpbb_topics` CHANGE COLUMN topic_title topic_title CHAR(60) CHARACTER SET 'utf8';

/**********PM SUBJECT ******************/
ALTER TABLE `phpbb_privmsgs` CHANGE COLUMN privmsgs_subject privmsgs_subject VARCHAR(100) CHARACTER SET 'binary';
UPDATE
    `phpbb_privmsgs` pm_t
  SET privmsgs_subject = @txt 
  WHERE 
  (char_length(privmsgs_subject) <>  LENGTH(@txt := CONVERT(CONVERT(privmsgs_subject USING latin1) USING utf8)))
  and
  (pm_t.privmsgs_date < 
   (select 
		posts_t.post_time
     from
     `phpbb_posts` posts_t
     where (posts_t.post_id = @FIRST_POST_AFTER)
   )
  ) /* all valid posts below new*/
  and
  (pm_t.privmsgs_id >= 1) /* safe mode compliance */
;
ALTER TABLE `phpbb_privmsgs` CHANGE COLUMN privmsgs_subject privmsgs_subject VARCHAR(100) CHARACTER SET 'utf8';

/**********PM TEXT BODY ******************/
ALTER TABLE `phpbb_privmsgs_text` CHANGE COLUMN privmsgs_text privmsgs_text TEXT CHARACTER SET 'binary';
UPDATE
    `phpbb_privmsgs_text` pm_t_t
  SET privmsgs_text = @txt 
  WHERE 
  (char_length(privmsgs_text) <>  LENGTH(@txt := CONVERT(CONVERT(privmsgs_text USING latin1) USING utf8)))
  and
  (pm_t_t.privmsgs_text_id in
   (select 
		pm_t.privmsgs_id
     from
	  `phpbb_privmsgs` pm_t
     inner join
     `phpbb_posts` posts_t
     on
     pm_t.privmsgs_date < posts_t.post_time
     where (posts_t.post_id = @FIRST_POST_AFTER)
   )
  ) /* all valid posts below new*/
  and
  (pm_t_t.privmsgs_text_id >= 1) /* safe mode compliance */
;
ALTER TABLE `phpbb_privmsgs_text` CHANGE COLUMN privmsgs_text privmsgs_text TEXT CHARACTER SET 'utf8';








