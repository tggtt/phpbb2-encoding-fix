/**
CONVERSION SCRIPT.
NOTICE: THE TABLE NAMES VARY.

This file is ASCII.

Just dealing with posts, privmsgs and topics.
Usernames should be updated separately, I do not want to risk breaking login.

Still, since last check, all new usernames only use ASCII letters.
Check if there:
http://ld4all.com/forum/memberlist.php?mode=joined&order=DESC&start=0

Defaults are:
1-  POSTS_TABLE or phpbb_posts
2-  TOPICS_TABLE or phpbb_topics
3-  POSTS_TEXT_TABLE or phpbb_posts_text

Important warning: I do not know if CHAR/VARCHAR length need to be increased to avoid issues with longer data in UTF-8 mode.
*/

SET @FIRST_POST_AFTER := (<<FIRST POST NUMBER AFTER MAINTENANCE HERE>>);


/*POSTS*/
/* playing safe: binary copy of new posts, to maintain their UTF-ness. */
create table utfposts_text_copy as
 select 
  post_t_t.post_id as post_id,
  CONVERT(cast(post_t_t.post_subject AS BINARY) USING utf8) as post_subject,
  CONVERT(cast(post_t_t.post_text AS BINARY) USING utf8) as post_text
from 
 `phpbb_posts_text` post_t_t
 left outer join
 `phpbb_posts` post_t
 on
 post_t_t.post_id = post_t.post_id
where
 post_t.post_time  /* SAME OR NEWER than projected first post*/
  >=
 (
  select post_time from `phpbb_posts` post_t_2 where (post_t_2.post_id = @FIRST_POST_AFTER)
 )
;
ALTER TABLE utfposts_text_copy ADD PRIMARY KEY (`post_id`);


/*TOPICS*/
/* playing safe: binary copy of new topics to maintain their UTF-ness. */
create table utftopic_title_copy as 
 select 
  topics_t.topic_id as topic_id,
  CONVERT(cast(topics_t.topic_title AS BINARY) USING utf8) as topic_title
from 
 `phpbb_topics` topics_t
where
 topics_t.topic_time  /* SAME OR NEWER than projected first post*/
  >=
 (
  select post_time from `phpbb_posts` post_t where (post_t.post_id = @FIRST_POST_AFTER)
 )
;
ALTER TABLE utftopic_title_copy ADD PRIMARY KEY (`topic_id`);

/*pm-subjects*/
/* playing safe: binary copy of new pms to maintain their UTF-ness. */
create table utfpm_subj_copy as 
 select 
  pm_t.privmsgs_id as privmsgs_id,
  CONVERT(cast(pm_t.privmsgs_subject AS BINARY) USING utf8) as privmsgs_subject
from 
 `phpbb_privmsgs` pm_t
where
 pm_t.privmsgs_date  /* SAME OR NEWER than projected first post*/
  >=
 (
  select post_time from `phpbb_posts` post_t where (post_t.post_id = @FIRST_POST_AFTER)
 )
;
ALTER TABLE utfpm_subj_copy ADD PRIMARY KEY (`privmsgs_id`);

/*pm-subjects*/
/* playing safe: binary copy of new pms to maintain their UTF-ness. */
create table utfpm_text_copy as 
 select 
  pm_t_t.privmsgs_text_id as privmsgs_text_id,
  CONVERT(cast(pm_t_t.privmsgs_text AS BINARY) USING utf8) as privmsgs_text
from 
 `phpbb_privmsgs_text` pm_t_t
where
 pm_t_t.privmsgs_text_id  /* SAME OR NEWER than projected first post*/
  IN
 (
   select 
		pm_t.privmsgs_id
     from
	  `phpbb_privmsgs` pm_t
     inner join
     `phpbb_posts` posts_t
     on
     pm_t.privmsgs_date >= posts_t.post_time
     where (posts_t.post_id = @FIRST_POST_AFTER)
 )
;
ALTER TABLE utfpm_text_copy ADD PRIMARY KEY (`privmsgs_text_id`);

/*ACTUAL CONVERSION*/

/*convert the whole posts_text table. */

/*warning: LD4all has more than 444000 posts, everything will be converted! */
/*warning: UTF posts would appear broken after this. */

/*NOTE: I know it sounds strange. BUT THIS SEEMS BETTER
I have tried to convert to binary and then only convert the old posts (CODE NUMBER 3),
but lots of warnings appear.
Apparently MySQL has some sort of automatic check for encoding,
it seems safer to convert the whole thing and
then rewrite the backup of posts that are already in UTF.*/

ALTER TABLE `phpbb_posts_text` CHANGE COLUMN post_subject post_subject CHAR(60) CHARACTER SET 'utf8';

ALTER TABLE `phpbb_posts_text` CHANGE COLUMN post_text post_text TEXT CHARACTER SET 'utf8';

ALTER TABLE `phpbb_topics` CHANGE COLUMN topic_title topic_title CHAR(60) CHARACTER SET 'utf8';

ALTER TABLE `phpbb_privmsgs` CHANGE COLUMN privmsgs_subject privmsgs_subject VARCHAR(100) CHARACTER SET 'utf8';

ALTER TABLE `phpbb_privmsgs_text` CHANGE COLUMN privmsgs_text privmsgs_text TEXT CHARACTER SET 'utf8';

/*
In case you are wondering, this would work for usernames:
ALTER TABLE `phpbb_users` CHANGE COLUMN username username VARCHAR(32) CHARACTER SET 'utf8';
The registration date column is user_regdate INT.
*/


/* BACKUP COPY BACK new topics and posts that were already in UTF-8, so they won't be broken anymore */
UPDATE `phpbb_posts_text` original
 set
 original.post_subject = 
 (select copy.post_subject from utfposts_text_copy copy
  where copy.post_id = original.post_id
 ),
 original.post_text = 
 (select copy.post_text from utfposts_text_copy copy
  where copy.post_id = original.post_id
 )
 where
 (original.post_id >= @FIRST_POST_AFTER)
 ;


/*copy back the new topic titles*/

UPDATE `phpbb_topics` original
 set
 original.topic_title =
 (select copy.topic_title from
	utftopic_title_copy copy
    where copy.topic_id = original.topic_id
 )
 where 
 (original.topic_id in (select copy.topic_id from utftopic_title_copy copy)) /* all copied topics */
 and
 (original.topic_id >= 1 ) /*all valid topics*/
 ;
 
/*copy back the new PM SUBJ*/

UPDATE `phpbb_privmsgs` original
 set
 original.privmsgs_subject =
 (select copy.privmsgs_subject from
	utfpm_subj_copy copy
    where copy.privmsgs_id = original.privmsgs_id
 )
 where 
 (original.privmsgs_id in (select copy.privmsgs_id from utfpm_subj_copy copy)) /* all copied topics */
 and
 (original.privmsgs_id >= 1 ) /*all valid topics*/
 ;

/*copy back the new PM TEXT*/
UPDATE `phpbb_privmsgs_text` original
 set
 original.privmsgs_text =
 (select copy.privmsgs_text from
	utfpm_text_copy copy
    where copy.privmsgs_text_id = original.privmsgs_text_id
 )
 where 
 (original.privmsgs_text_id in (select copy.privmsgs_text_id from utfpm_text_copy copy)) /* all copied topics */
 and
 (original.privmsgs_text_id >= 1 ) /*all valid topics*/
 ;

/* WE CAN NOW DELETE THE REDUNDANT BACKUPS*/ 
drop table utfposts_text_copy;

drop table utftopic_title_copy;

drop table utfpm_subj_copy;

drop table utfpm_text_copy;